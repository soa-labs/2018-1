# SOA labs: 1. Intro to Docker
## Dockerize spring app, use docker-compose and mysql

### Reproduce steps

1. Clone the project
   * `git clone git@gitlab.com:soa-labs/2018-1.git`

2. Build the project and the docker images:
   * `mvn package docker:build`

3. Start mysql (and wait for it to start):
   * `docker-compose up -d mysql`

4. Ensure mysql container is up and running:
   * docker ps
   * docker logs <container-id> 

5. Start other containers defined in docker-compose:
   * `docker-compose up -d`

6. Test the application:
   * `curl http://127.0.0.1/greeting`
   * use *Postman* to test *CustomerController*

7. Scale application with 3 instances:
   * `docker-compose scale app=3`

8. Check Traefik web-panel:
   * `http://127.0.0.1:8080`

9. Downscale application with 1 instance:
   * `docker-compose scale app=1`

10. Shutdown containers:
   * `docker-compose down`
